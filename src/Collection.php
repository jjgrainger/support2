<?php

namespace Support;

use Countable;
use ArrayIterator;
use JsonSerializable;
use IteratorAggregate;
use InvalidArgumentException;

/**
 * Meant for non-associative arrays
 */
class Collection implements Countable, IteratorAggregate, JsonSerializable
{

    /**
     * The collection items
     * @var array
     */
    protected $items;

    function __construct(array $items = [])
    {
        $this->items = $items;
    }

    /**
     * Check if key exists in collection
     * @param  mixed $key
     * @return boolean
     */
    function has($key)
    {
        return isset($this->items[$key]);
    }

    /**
     * Get an item in the collection by its key
     * @param  mixed $key
     * @return mixed
     */
    function get($key, $default = null)
    {
        if ($this->has($key)) {
            return $this->items[$key];
        }

        return $default;
    }

    /**
     * Return all items in the collection
     * @return array
     */
    function all()
    {
        return $this->items;
    }

    /**
     * Return the first item in the collection
     * @return mixed
     */
    function first()
    {
        if (!$this->isEmpty()) {
            return $this->items[0];
        }

        return null;
    }

    /**
     * Return the last item in the collection
     * @return mixed
     */
    function last()
    {
        if (!$this->isEmpty()) {
            return $this->items[count($this->items)-1];
        }

        return null;
    }

    /**
     * Check to see if the collection is empty
     * @return boolean
     */
    function isEmpty()
    {
        return empty($this->items);
    }

    /**
     * Remove and return the first item in the collection
     * @return mixed
     */
    function shift()
    {
        return array_shift($this->items);
    }

    /**
     * Remote and return the last item in the collection
     * @return [type] [description]
     */
    function pop()
    {
        return array_pop($this->items);
    }

    /**
     * Add an additional item to the collection
     * @param mixed $items
     */
    function add($item)
    {
        $this->items[] = $item;

        return $this;
    }

    /**
     * Add an additional item to the collection at the beginning
     * @param  mixed $item
     */
    function prepend($item)
    {
        array_unshift($this->items, $item);

        return $this;
    }

    /**
     * Return number of items in the collection
     * @return int
     */
    function count()
    {
        return count($this->items);
    }

    /**
     * Return random items from the collection
     * @param  int $num number of items to return
     * @return mixed
     */
    function random(int $amount = 1)
    {
        if ($this->isEmpty()) {
            return null;
        }

        if ($amount > ($count = $this->count())) {
            throw new InvalidArgumentException("You requested {$amount} items, but there are only {$count} items in the collection");
        }

        $keys = array_rand($this->items, $amount);

        if (is_array($keys)) {
            return array_intersect_key($this->items, array_flip($keys));
        }

        return $this->get($keys);
    }

    /**
     * Reverse the items in the collection
     */
    function reverse()
    {
        $this->items = array_reverse($this->items);
    }

    /**
     * Shuffle the items in the collection
     */
    function shuffle()
    {
        $this->items = shuffle($this->items);
    }

    /**
     * Empty the collection
     */
    function clear()
    {
        $this->items = [];
    }

    /**
     * Replace collection items with a new set
     * @param  array  $items
     */
    function replace(array $items)
    {
        $this->items = $items;
    }

    /**
     * Return the collection as JSON
     * @param  int $options
     * @return string
     */
    function toJson($options = 0)
    {
        return json_encode($this->items, $options);
    }

    /**
     * Iterate over items in collection
     */
    public function getIterator()
    {
        return new ArrayIterator($this->items);
    }

    /**
     * JSON Serialize items in the collection
     */
    public function jsonSerialize()
    {
        return $this->items;
    }
}
