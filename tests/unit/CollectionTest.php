<?php

class CollectionTest extends \PHPUnit_Framework_TestCase
{

    //-------------------------------------------------------
    // Collection instantiation tests
    //-------------------------------------------------------

    public function test_collection_is_empty_when_instantiated()
    {
        $collection = new \Support\Collection;

        $this->assertEmpty($collection->all());
        $this->assertEquals($collection->count(), 0);
    }

    public function test_items_returned_match_items_instantiated_with()
    {
        $collection = new \Support\Collection(['one', 'two', 'three']);

        $items = $collection->all();

        $this->assertEquals($items[0], 'one');
        $this->assertEquals($items[1], 'two');
        $this->assertEquals($items[2], 'three');
    }

    //-------------------------------------------------------
    // Has method tests
    //-------------------------------------------------------

    public function test_has_returns_true_when_item_exists_in_collection()
    {
        $collection = new \Support\Collection(['one', 'two', 'three']);

        $this->assertTrue($collection->has(0));
        $this->assertTrue($collection->has(1));
        $this->assertTrue($collection->has(2));
    }

    public function test_has_returns_false_on_empty_collection()
    {
        $collection = new \Support\Collection;

        $this->assertFalse($collection->has(0));
        $this->assertFalse($collection->has(1));
        $this->assertFalse($collection->has(2));
    }

    //-------------------------------------------------------
    // Get method tests
    //-------------------------------------------------------

    public function test_get_returns_correct_items()
    {
        $collection = new \Support\Collection(['one', 'two', 'three']);

        $this->assertEquals($collection->get(0), 'one');
        $this->assertEquals($collection->get(1), 'two');
        $this->assertEquals($collection->get(2), 'three');
    }

    public function test_get_returns_default_on_items_not_found()
    {
        $collection = new \Support\Collection(['one', 'two', 'three']);

        $this->assertNull($collection->get(4));
        $this->assertEquals($collection->get(4, 'default'), 'default');
    }

    //-------------------------------------------------------
    // All method tests
    //-------------------------------------------------------

    function test_all_returns_empty_array_for_empty_collection()
    {
        $collection = new \Support\Collection;

        $this->assertEmpty($collection->all());
    }

    function test_all_returns_all_items_in_collection()
    {
        $collection = new \Support\Collection(['one', 'two', 'three']);

        $this->assertEquals($collection->all(), ['one', 'two', 'three']);
    }

    //-------------------------------------------------------
    // First method tests
    //-------------------------------------------------------

    public function test_first_returns_first_item()
    {
        $collection = new \Support\Collection(['one', 'two', 'three']);

        $this->assertEquals($collection->first(), 'one');
    }

    public function test_first_returns_null_on_empty_collection()
    {
        $collection = new \Support\Collection;

        $this->assertNull($collection->first());
    }

    //-------------------------------------------------------
    // Last method tests
    //-------------------------------------------------------

    public function test_last_returns_last_item()
    {
        $collection = new \Support\Collection(['one', 'two', 'three']);

        $this->assertEquals($collection->last(), 'three');
    }

    public function test_last_returns_null_on_empty_collection()
    {
        $collection = new \Support\Collection;

        $this->assertNull($collection->last());
    }

    //-------------------------------------------------------
    // isEmpty method tests
    //-------------------------------------------------------

    public function test_isEmpty_is_true_for_empty_collection()
    {
        $collection = new \Support\Collection;

        $this->assertTrue($collection->isEmpty());
    }

    public function test_isEmpty_is_false_for_collection_with_items()
    {
        $collection = new \Support\Collection(['one', 'two', 'three']);

        $this->assertFalse($collection->isEmpty());
    }

    //-------------------------------------------------------
    // Shift method tests
    //-------------------------------------------------------

    public function test_shift_returns_first_item()
    {
        $collection = new \Support\Collection(['one', 'two', 'three']);

        $item = $collection->shift();

        $this->assertEquals($item, 'one');
    }

    public function test_shift_removes_first_item()
    {
        $collection = new \Support\Collection(['one', 'two', 'three']);

        $item = $collection->shift();

        $this->assertEquals($collection->count(), 2);
        $this->assertNotContains('one', $collection->all());
    }

    public function test_shift_returns_null_on_empty_collection()
    {
        $collection = new \Support\Collection;

        $item = $collection->shift();

        $this->assertNull($item);
    }

    //-------------------------------------------------------
    // Pop method tests
    //-------------------------------------------------------

    public function test_pop_returns_first_item()
    {
        $collection = new \Support\Collection(['one', 'two', 'three']);

        $item = $collection->pop();

        $this->assertEquals($item, 'three');
    }

    public function test_pop_removes_first_item()
    {
        $collection = new \Support\Collection(['one', 'two', 'three']);

        $item = $collection->pop();

        $this->assertEquals($collection->count(), 2);
        $this->assertNotContains('three', $collection->all());
    }

    public function test_pop_returns_null_on_empty_collection()
    {
        $collection = new \Support\Collection;

        $item = $collection->pop();

        $this->assertNull($item);
    }

    //-------------------------------------------------------
    // Add method tests
    //-------------------------------------------------------

    public function test_add_item_to_collection()
    {
        $collection = new \Support\Collection(['one', 'two', 'three']);

        $collection->add('four');

        $this->assertEquals($collection->count(), 4);
        $this->assertContains('four', $collection->all());
    }

    public function test_add_appends_item_to_collection()
    {
        $collection = new \Support\Collection(['one', 'two', 'three']);

        $collection->add('four');

        $this->assertEquals($collection->last(), 'four');
    }

    function test_add_to_empty_collection()
    {
        $collection = new \Support\Collection;

        $collection->add('one');

        $this->assertEquals($collection->count(), 1);
        $this->assertEquals($collection->last(), 'one');
    }

    //-------------------------------------------------------
    // Prepend method tests
    //-------------------------------------------------------

    public function test_prepend_item_to_collection()
    {
        $collection = new \Support\Collection(['one', 'two', 'three']);

        $collection->prepend('zero');

        $this->assertEquals($collection->count(), 4);
        $this->assertContains('zero', $collection->all());
    }

    public function test_prepend_adds_item_at_beginning_of_collection()
    {
        $collection = new \Support\Collection(['one', 'two', 'three']);

        $collection->prepend('zero');

        $this->assertEquals($collection->first(), 'zero');
    }

    function test_prepend_to_empty_collection()
    {
        $collection = new \Support\Collection;

        $collection->prepend('zero');

        $this->assertEquals($collection->count(), 1);
        $this->assertEquals($collection->first(), 'zero');
    }


    //-------------------------------------------------------
    // Count method tests
    //-------------------------------------------------------

    public function test_count_is_correct_for_items_passed()
    {
        $collection = new \Support\Collection(['one', 'two', 'three']);

        $this->assertEquals($collection->count(), 3);
    }

    public function test_count_is_zero_for_empty_collection()
    {
        $collection = new \Support\Collection;

        $this->assertEquals($collection->count(), 0);
    }

    public function test_collection_object_is_countable()
    {
        $collection = new \Support\Collection(['one', 'two', 'three']);

        $this->assertEquals(count($collection), 3);
    }

    //-------------------------------------------------------
    // Random method tests
    //-------------------------------------------------------

    function test_random_returns_an_item_in_collection()
    {
        $collection = new \Support\Collection(['one', 'two', 'three']);

        $this->assertContains($collection->random(), $collection->all());
    }

    function test_random_returns_null_on_empty_collection()
    {
        $collection = new \Support\Collection;

        $this->assertNull($collection->random());
    }

    function test_random_provides_same_number_items_as_amount()
    {
        $collection = new \Support\Collection(['one', 'two', 'three']);

        $items = $collection->random(2);

        $this->assertEquals(count($items), 2);
    }

    function test_random_throws_exception_when_amount_is_greater_than_items_count()
    {
        $this->expectException(InvalidArgumentException::class);

        $collection = new \Support\Collection(['one', 'two', 'three']);

        $collection->random(4);
    }

    //-------------------------------------------------------
    // Reverse method tests
    //-------------------------------------------------------

    //-------------------------------------------------------
    // Shuffle method tests
    //-------------------------------------------------------

    //-------------------------------------------------------
    // Clear method tests
    //-------------------------------------------------------

    //-------------------------------------------------------
    // Replace method tests
    //-------------------------------------------------------

    //-------------------------------------------------------
    // toJson method tests
    //-------------------------------------------------------

    //-------------------------------------------------------
    // ArrayIterator tests
    //-------------------------------------------------------

    public function collection_can_be_iterated()
    {
        $collection = new \Support\Collection(['one', 'two', 'three']);

        $items = [];

        foreach ($collection as $item) {
            $items[] = $item;
        }

        $this->assertCount(3, $items);
        $this->assertInstanceOf(ArrayIterator::class, $collection->getIterator());
    }
}
