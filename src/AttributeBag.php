<?php

namespace Support;

/**
 * Meant for associative arrays (key => value pairs)
 */
class AttributeBag
{

    protected $items;

    function __construct(array $items = [])
    {
        $this->items = $items;
    }

    function has($key)
    {
        return isset($this->items[$key]);
    }

    function get($key, $default = null)
    {
        if ($this->has($key)) {
            return $this->items[$key];
        }

        return $default;
    }

    function set($key, $value)
    {
        $this->items[$key] = $value;
    }

    function all()
    {
        return $this->items;
    }

    function count()
    {
        return count($this->items);
    }

    function empty()
    {
        $this->items = [];
    }

    function toJson($options = 0)
    {
        return json_encode($this->items, $options);
    }


}
